#! /bash/bin/python
import csv
import os
import requests


class Soutient():
    def __init__(self, nom, prenom, fonction):
        self.nom = nom
        self.prenom = prenom
        self.fonction = fonction
        self.circonscription = ''
        self.departement = ''

    def __str__(self):
        return f"{self.nom} {self.prenom}"


class Candidat():
    def __init__(self, name):
        self.name = name
        self.soutient = []
        self.count = 1

    def add(self, objetSoutient):
        self.count += 1
        self.soutient.append(objetSoutient)


def download(URL, filename):
    """
    Récupère le CSV depuis le web
    """
    # Check file
    response = requests.get(URL)
    if response.status_code == 200:
        filecsv = open(filename, mode='wb')
        print("Download file")
        size = filecsv.write(response.content)
        filecsv.close()
    elif response.status_code == 404:
        print("404 : File not found")
        size = 0
    else:
        print(f"Error : {response.status_code}")
        size = 0
    return size


def parse(filename):
    """
    Parse le fichier CSV
    """
    # head : Civilité;Nom;Prénom;Mandat;Circonscription;Département;Candidat;"Date de publication"
    soutientsDict = {}
    print(f"parsing {filename}")
    with open(filename, 'r') as filecsv:
        reader = csv.DictReader(filecsv, delimiter=';')
        for row in reader:
            if row['Candidat'] not in soutientsDict:
                soutientsDict[row['Candidat']] = Candidat(row['Candidat'])
            newSoutient = Soutient(row['Nom'], row['Prénom'], row['Mandat'])
            soutientsDict[row['Candidat']].add(newSoutient)
    return soutientsDict


def display(dict):
    """
    Affichage des résultats
    """
    body = ''
    maxEspace = 0
    # Espace et mise en page
    for nomCandidat in dict:
        if len(nomCandidat) > maxEspace:
            maxEspace = len(nomCandidat)
    espace = ' ' * (maxEspace - len('Candidat'))
    header = f"Candidat{espace}\tNombre\n"
    # display
    dict_sorted = sorted(dict.items(), key=lambda item: item[1].count, reverse=True)
    total = 0
    for nomCandidat in dict_sorted:
        espace = ' ' * (maxEspace - len(nomCandidat[0]))
        body += f"{nomCandidat[0]}{espace}\t{nomCandidat[1].count}\n"
        total += nomCandidat[1].count
    espace = ' ' * (maxEspace - len('Total'))
    footer = f"Total{espace}\t{total}\n"
    txt = header + body + footer
    return txt


if __name__ == '__main__':
    URL = "https://presidentielle2022.conseil-constitutionnel.fr/telechargement/parrainagestotal.csv"
    FILE = 'parrainage.csv'
    FILE_TMP = 'parrainage_tmp.csv'
    # Get file
    if not os.path.exists(FILE):
        # Download file
        print("Downloading file…")
        size = download(URL, FILE)
    else:
        print("File exist, check size / diff")
        # Check size
        oldsize = os.path.getsize(FILE)
        newsize = download(URL, FILE_TMP)
        if newsize != oldsize:
            os.rename(FILE_TMP, FILE)
        else:
            print("File size no change")
    # Parsing file
    parrainagesListe = parse(FILE)
    print(display(parrainagesListe))
